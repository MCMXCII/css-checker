# Style Checker

Ever needed a quick and easier way to see the differences between your development and project stylesheets.

Simply enter the path to your stylesheets in the shell script and move the file in a bin directory in your $PATH variable.
