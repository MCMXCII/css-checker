#!/bin/sh
# URL paths to stylesheets, for example, http://www.domain.com/skin/css/styles.css
LIVE=""
LOCAL=""

# Check Dependencies
if !(type wget) &> /dev/null
	then
		echo "wget binary not found, please install it. Please run 'npm install -g wget'."
		exit
fi

if !(type cssunminifier) &> /dev/null
	then
		echo "cssunminifier binary not found, please install it. Please run 'npm install -g cssunminifier'."
		exit
fi

# Setup Temp Directory
mkdir .style_comparison
cd .style_comparison

# Pull Live File
wget -q -O live.css $LIVE

# Grab Local File
wget -q -O local.css $LOCAL

# Beautify Both Into Temps
cssunminifier live.css live.css
cssunminifier local.css local.css

# Output Diff Into File
diff -u local.css live.css > ../output.patch

# Clean Up
cd ../
rm -rf .style_comparison
open output.patch